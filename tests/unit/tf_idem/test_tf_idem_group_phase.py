import pytest
import yaml


@pytest.mark.asyncio
async def test_tf_idem_group_phase(hub, change_test_dir):
    run_name = hub.OPT.idem_codegen.run_name
    hub.tf_idem.RUNS["FILTERED_SLS_DATA"] = yaml.safe_load(
        open(
            f"{hub.test.idem_codegen.current_path}/resources/idem_describe_response.sls"
        )
    )
    hub.test.idem_codegen.unit_test = False
    hub.idem_codegen.compiler.init.compile(run_name)
    hub.idem_codegen.group[hub.OPT.idem_codegen.group_style].segregate(run_name)
