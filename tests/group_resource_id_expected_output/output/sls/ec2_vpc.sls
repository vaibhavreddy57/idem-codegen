vpc-0738f2a523f4735bd:
  aws.ec2.vpc.present:
  - name: vpc-0738f2a523f4735bd
  - resource_id: vpc-0738f2a523f4735bd
  - instance_tenancy: default
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-cluster-node
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-02ab4e7064a606d2b
      CidrBlock: 10.170.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true


vpc-08f76fe175071e969:
  aws.ec2.vpc.present:
  - name: vpc-08f76fe175071e969
  - resource_id: vpc-08f76fe175071e969
  - instance_tenancy: default
  - tags:
    - Key: Name3
      Value: vijay-vpc-test3
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-054ac0d8549bbd88b
      CidrBlock: 10.1.150.0/28
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: false
  - enable_dns_support: true


vpc-0dbee6437661777d4:
  aws.ec2.vpc.present:
  - name: vpc-0dbee6437661777d4
  - resource_id: vpc-0dbee6437661777d4
  - instance_tenancy: default
  - tags:
    - Key: Name
      Value: idem-fixture-vpc-61ddff14-631a-4ee9-bab6-daa191a7da66
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-054669469e1afac05
      CidrBlock: 10.0.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: false
  - enable_dns_support: true


vpc-dcae57b5:
  aws.ec2.vpc.present:
  - name: vpc-dcae57b5
  - resource_id: vpc-dcae57b5
  - instance_tenancy: default
  - tags:
    - Key: Name
      Value: default
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-5e854037
      CidrBlock: 172.31.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true
