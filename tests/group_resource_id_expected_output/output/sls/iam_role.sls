AWS-QuickSetup-StackSet-Local-AdministrationRole:
  aws.iam.role.present:
  - resource_id: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - name: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - arn: arn:aws:iam::123456789012:role/AWS-QuickSetup-StackSet-Local-AdministrationRole
  - id: AROAX2FJ77DCYHZJS2UUV
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "cloudformation.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWS-QuickSetup-StackSet-Local-ExecutionRole:
  aws.iam.role.present:
  - resource_id: AWS-QuickSetup-StackSet-Local-ExecutionRole
  - name: AWS-QuickSetup-StackSet-Local-ExecutionRole
  - arn: arn:aws:iam::123456789012:role/AWS-QuickSetup-StackSet-Local-ExecutionRole
  - id: AROAX2FJ77DCTRT7XN4BI
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:role/AWS-QuickSetup-StackSet-Local-AdministrationRole"}}],
      "Version": "2012-10-17"}'


AWSServiceRoleForAWSCloud9:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAWSCloud9
  - name: AWSServiceRoleForAWSCloud9
  - arn: arn:aws:iam::123456789012:role/aws-service-role/cloud9.amazonaws.com/AWSServiceRoleForAWSCloud9
  - id: AROAX2FJ77DCWO6CED7WB
  - path: /aws-service-role/cloud9.amazonaws.com/
  - description: Service linked role for AWS Cloud9
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "cloud9.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonBraket:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonBraket
  - name: AWSServiceRoleForAmazonBraket
  - arn: arn:aws:iam::123456789012:role/aws-service-role/braket.amazonaws.com/AWSServiceRoleForAmazonBraket
  - id: AROAX2FJ77DC3NDCJUSOE
  - path: /aws-service-role/braket.amazonaws.com/
  - description: Service role created by Amazon Braket Console
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "braket.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonElasticFileSystem:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonElasticFileSystem
  - name: AWSServiceRoleForAmazonElasticFileSystem
  - arn: arn:aws:iam::123456789012:role/aws-service-role/elasticfilesystem.amazonaws.com/AWSServiceRoleForAmazonElasticFileSystem
  - id: AROAX2FJ77DCZH5XYF76X
  - path: /aws-service-role/elasticfilesystem.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "elasticfilesystem.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForAmazonGuardDuty:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonGuardDuty
  - name: AWSServiceRoleForAmazonGuardDuty
  - arn: arn:aws:iam::123456789012:role/aws-service-role/guardduty.amazonaws.com/AWSServiceRoleForAmazonGuardDuty
  - id: AROAX2FJ77DCZCOCKFNFT
  - path: /aws-service-role/guardduty.amazonaws.com/
  - description: 'A service-linked role required for Amazon GuardDuty to access your
      resources. '
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "guardduty.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonMQ:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonMQ
  - name: AWSServiceRoleForAmazonMQ
  - arn: arn:aws:iam::123456789012:role/aws-service-role/mq.amazonaws.com/AWSServiceRoleForAmazonMQ
  - id: AROAX2FJ77DCRRO6Y53RR
  - path: /aws-service-role/mq.amazonaws.com/
  - description: Allows Amazon MQ to call AWS services on your behalf
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "mq.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonOpenSearchService:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonOpenSearchService
  - name: AWSServiceRoleForAmazonOpenSearchService
  - arn: arn:aws:iam::123456789012:role/aws-service-role/opensearchservice.amazonaws.com/AWSServiceRoleForAmazonOpenSearchService
  - id: AROAX2FJ77DCQWW7SGRAB
  - path: /aws-service-role/opensearchservice.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "opensearchservice.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForAmazonSSM:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonSSM
  - name: AWSServiceRoleForAmazonSSM
  - arn: arn:aws:iam::123456789012:role/aws-service-role/ssm.amazonaws.com/AWSServiceRoleForAmazonSSM
  - id: AROAX2FJ77DCU3AAWCVYJ
  - path: /aws-service-role/ssm.amazonaws.com/
  - description: Provides access to AWS Resources managed or used by Amazon SSM.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ssm.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonxyz:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonxyz
  - name: AWSServiceRoleForAmazonxyz
  - arn: arn:aws:iam::123456789012:role/aws-service-role/xyz.amazonaws.com/AWSServiceRoleForAmazonxyz
  - id: AROAX2FJ77DC75XAPS2YS
  - path: /aws-service-role/xyz.amazonaws.com/
  - description: Allows Amazon xyz to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonxyzForFargate:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonxyzForFargate
  - name: AWSServiceRoleForAmazonxyzForFargate
  - arn: arn:aws:iam::123456789012:role/aws-service-role/xyz-fargate.amazonaws.com/AWSServiceRoleForAmazonxyzForFargate
  - id: AROAX2FJ77DCRGULKS6PV
  - path: /aws-service-role/xyz-fargate.amazonaws.com/
  - description: This policy grants necessary permissions to Amazon xyz to run fargate
      tasks
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz-fargate.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForAmazonxyzNodegroup:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonxyzNodegroup
  - name: AWSServiceRoleForAmazonxyzNodegroup
  - arn: arn:aws:iam::123456789012:role/aws-service-role/xyz-nodegroup.amazonaws.com/AWSServiceRoleForAmazonxyzNodegroup
  - id: AROAX2FJ77DCWMKF5DRTO
  - path: /aws-service-role/xyz-nodegroup.amazonaws.com/
  - description: This policy allows Amazon xyz to create and manage Nodegroups
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz-nodegroup.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForApplicationAutoScaling_DynamoDBTable:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
  - name: AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
  - arn: arn:aws:iam::123456789012:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
  - id: AROAX2FJ77DCWYM4GYO65
  - path: /aws-service-role/dynamodb.application-autoscaling.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "dynamodb.application-autoscaling.amazonaws.com"}}],
      "Version": "2012-10-17"}'


AWSServiceRoleForAutoScaling:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAutoScaling
  - name: AWSServiceRoleForAutoScaling
  - arn: arn:aws:iam::123456789012:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling
  - id: AROAIXAVZRCZRYFDGCIME
  - path: /aws-service-role/autoscaling.amazonaws.com/
  - description: Default Service-Linked Role enables access to AWS Services and Resources
      used or managed by Auto Scaling
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "autoscaling.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForBackup:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForBackup
  - name: AWSServiceRoleForBackup
  - arn: arn:aws:iam::123456789012:role/aws-service-role/backup.amazonaws.com/AWSServiceRoleForBackup
  - id: AROAX2FJ77DCXHBSDTACQ
  - path: /aws-service-role/backup.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "backup.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForCloudWatchEvents:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForCloudWatchEvents
  - name: AWSServiceRoleForCloudWatchEvents
  - arn: arn:aws:iam::123456789012:role/aws-service-role/events.amazonaws.com/AWSServiceRoleForCloudWatchEvents
  - id: AROAX2FJ77DCWZCEVKZS4
  - path: /aws-service-role/events.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "events.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForComputeOptimizer:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForComputeOptimizer
  - name: AWSServiceRoleForComputeOptimizer
  - arn: arn:aws:iam::123456789012:role/aws-service-role/compute-optimizer.amazonaws.com/AWSServiceRoleForComputeOptimizer
  - id: AROAX2FJ77DCRUVCZE4XO
  - path: /aws-service-role/compute-optimizer.amazonaws.com/
  - description: Allows ComputeOptimizer to call AWS services and collect workload
      details on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "compute-optimizer.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForEC2Spot:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForEC2Spot
  - name: AWSServiceRoleForEC2Spot
  - arn: arn:aws:iam::123456789012:role/aws-service-role/spot.amazonaws.com/AWSServiceRoleForEC2Spot
  - id: AROAIWYJPB6SSVRYB2JBK
  - path: /aws-service-role/spot.amazonaws.com/
  - description: Default EC2 Spot Service Linked Role
  - max_session_duration: 3600
  - tags:
    - Key: tag-key
      Value: tag-value
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForEC2SpotFleet:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForEC2SpotFleet
  - name: AWSServiceRoleForEC2SpotFleet
  - arn: arn:aws:iam::123456789012:role/aws-service-role/spotfleet.amazonaws.com/AWSServiceRoleForEC2SpotFleet
  - id: AROAJCTOK4KQO3LXR24PU
  - path: /aws-service-role/spotfleet.amazonaws.com/
  - description: Default EC2 Spot Fleet Service Linked Role
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spotfleet.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForECS:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForECS
  - name: AWSServiceRoleForECS
  - arn: arn:aws:iam::123456789012:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS
  - id: AROAX2FJ77DCQGWLDN5BE
  - path: /aws-service-role/ecs.amazonaws.com/
  - description: Role to enable Amazon ECS to manage your cluster.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ecs.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForEMRCleanup:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForEMRCleanup
  - name: AWSServiceRoleForEMRCleanup
  - arn: arn:aws:iam::123456789012:role/aws-service-role/elasticmapreduce.amazonaws.com/AWSServiceRoleForEMRCleanup
  - id: AROAJCAGNAXQNDAFJ6OAY
  - path: /aws-service-role/elasticmapreduce.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "elasticmapreduce.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForElastiCache:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForElastiCache
  - name: AWSServiceRoleForElastiCache
  - arn: arn:aws:iam::123456789012:role/aws-service-role/elasticache.amazonaws.com/AWSServiceRoleForElastiCache
  - id: AROAX2FJ77DCRG7TXWCIC
  - path: /aws-service-role/elasticache.amazonaws.com/
  - description: This policy allows ElastiCache to manage AWS resources on your behalf
      as necessary for managing your cache.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "elasticache.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForElasticLoadBalancing:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForElasticLoadBalancing
  - name: AWSServiceRoleForElasticLoadBalancing
  - arn: arn:aws:iam::123456789012:role/aws-service-role/elasticloadbalancing.amazonaws.com/AWSServiceRoleForElasticLoadBalancing
  - id: AROAIHBDLHAYY6XX5SPEK
  - path: /aws-service-role/elasticloadbalancing.amazonaws.com/
  - description: Allows ELB to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "elasticloadbalancing.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForGlobalAccelerator:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForGlobalAccelerator
  - name: AWSServiceRoleForGlobalAccelerator
  - arn: arn:aws:iam::123456789012:role/aws-service-role/globalaccelerator.amazonaws.com/AWSServiceRoleForGlobalAccelerator
  - id: AROAX2FJ77DC2OLFBN3WS
  - path: /aws-service-role/globalaccelerator.amazonaws.com/
  - description: Allows Global Accelerator to call AWS services on customer's behalf
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "globalaccelerator.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForImageBuilder:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForImageBuilder
  - name: AWSServiceRoleForImageBuilder
  - arn: arn:aws:iam::123456789012:role/aws-service-role/imagebuilder.amazonaws.com/AWSServiceRoleForImageBuilder
  - id: AROAX2FJ77DCXMK7OH4PI
  - path: /aws-service-role/imagebuilder.amazonaws.com/
  - description: Allows EC2 Image Builder to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "imagebuilder.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForKafka:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForKafka
  - name: AWSServiceRoleForKafka
  - arn: arn:aws:iam::123456789012:role/aws-service-role/kafka.amazonaws.com/AWSServiceRoleForKafka
  - id: AROAX2FJ77DC4WCTGXP4I
  - path: /aws-service-role/kafka.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "kafka.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForKeyManagementServiceMultiRegionKeys:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForKeyManagementServiceMultiRegionKeys
  - name: AWSServiceRoleForKeyManagementServiceMultiRegionKeys
  - arn: arn:aws:iam::123456789012:role/aws-service-role/mrk.kms.amazonaws.com/AWSServiceRoleForKeyManagementServiceMultiRegionKeys
  - id: AROAX2FJ77DC3DCDPV2QL
  - path: /aws-service-role/mrk.kms.amazonaws.com/
  - description: Enables access to AWS services and resources required for AWS KMS
      Multi-Region Keys
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "mrk.kms.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForOrganizations:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForOrganizations
  - name: AWSServiceRoleForOrganizations
  - arn: arn:aws:iam::123456789012:role/aws-service-role/organizations.amazonaws.com/AWSServiceRoleForOrganizations
  - id: AROAJOCZGPKD6WKT7USIS
  - path: /aws-service-role/organizations.amazonaws.com/
  - description: Service-linked role used by AWS Organizations to enable integration
      of other AWS services with Organizations.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "organizations.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForRDS:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForRDS
  - name: AWSServiceRoleForRDS
  - arn: arn:aws:iam::123456789012:role/aws-service-role/rds.amazonaws.com/AWSServiceRoleForRDS
  - id: AROAJ6IHAHKHCDB3EVZIS
  - path: /aws-service-role/rds.amazonaws.com/
  - description: Allows Amazon RDS to manage AWS resources on your behalf
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "rds.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForRedshift:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForRedshift
  - name: AWSServiceRoleForRedshift
  - arn: arn:aws:iam::123456789012:role/aws-service-role/redshift.amazonaws.com/AWSServiceRoleForRedshift
  - id: AROAIW7EHPQGLFP2UBK2Q
  - path: /aws-service-role/redshift.amazonaws.com/
  - description: 'Allows Amazon Redshift to call AWS services on your behalf '
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "redshift.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForSecurityHub:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForSecurityHub
  - name: AWSServiceRoleForSecurityHub
  - arn: arn:aws:iam::123456789012:role/aws-service-role/securityhub.amazonaws.com/AWSServiceRoleForSecurityHub
  - id: AROAX2FJ77DC6HB5D4OFO
  - path: /aws-service-role/securityhub.amazonaws.com/
  - description: A service-linked role required for AWS Security Hub to access your
      resources.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "securityhub.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForSupport:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForSupport
  - name: AWSServiceRoleForSupport
  - arn: arn:aws:iam::123456789012:role/aws-service-role/support.amazonaws.com/AWSServiceRoleForSupport
  - id: AROAJ3J5Q3TRIL5PZGZWU
  - path: /aws-service-role/support.amazonaws.com/
  - description: Enables resource access for AWS to provide billing, administrative
      and support services
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "support.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForTrustedAdvisor:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForTrustedAdvisor
  - name: AWSServiceRoleForTrustedAdvisor
  - arn: arn:aws:iam::123456789012:role/aws-service-role/trustedadvisor.amazonaws.com/AWSServiceRoleForTrustedAdvisor
  - id: AROAIJQ7FEPUSQL3T3YYM
  - path: /aws-service-role/trustedadvisor.amazonaws.com/
  - description: Access for the AWS Trusted Advisor Service to help reduce cost, increase
      performance, and improve security of your AWS environment.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "trustedadvisor.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForVPCTransitGateway:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForVPCTransitGateway
  - name: AWSServiceRoleForVPCTransitGateway
  - arn: arn:aws:iam::123456789012:role/aws-service-role/transitgateway.amazonaws.com/AWSServiceRoleForVPCTransitGateway
  - id: AROAX2FJ77DCVXXA2PYYX
  - path: /aws-service-role/transitgateway.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "transitgateway.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AmazonSageMaker-ExecutionRole-20180207T165162:
  aws.iam.role.present:
  - resource_id: AmazonSageMaker-ExecutionRole-20180207T165162
  - name: AmazonSageMaker-ExecutionRole-20180207T165162
  - arn: arn:aws:iam::123456789012:role/service-role/AmazonSageMaker-ExecutionRole-20180207T165162
  - id: AROAJUMA4U5Z4IPZSBSYG
  - path: /service-role/
  - description: SageMaker execution role created from the SageMaker AWS Management
      Console.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "sagemaker.amazonaws.com"}}], "Version": "2012-10-17"}'


AmazonxyzEBSCSIRole:
  aws.iam.role.present:
  - resource_id: AmazonxyzEBSCSIRole
  - name: AmazonxyzEBSCSIRole
  - arn: arn:aws:iam::123456789012:role/AmazonxyzEBSCSIRole
  - id: AROAX2FJ77DCTHM2E6CSP
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"StringEquals": {"oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:aud":
      "sts.amazonaws.com"}}, "Effect": "Allow", "Principal": {"Federated": "arn:aws:iam::123456789012:oidc-provider/oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A"}}],
      "Version": "2012-10-17"}'


CloudHealth_Borathon:
  aws.iam.role.present:
  - resource_id: CloudHealth_Borathon
  - name: CloudHealth_Borathon
  - arn: arn:aws:iam::123456789012:role/CloudHealth_Borathon
  - id: AROAX2FJ77DC6M62R6UOP
  - path: /
  - description: CHT_CAS Borathon
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "97c21f537f673e1419a4d11b882dad"}}, "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::454464851268:root"}}], "Version":
      "2012-10-17"}'


CloudTrail_CloudWatchLogs_Role:
  aws.iam.role.present:
  - resource_id: CloudTrail_CloudWatchLogs_Role
  - name: CloudTrail_CloudWatchLogs_Role
  - arn: arn:aws:iam::123456789012:role/CloudTrail_CloudWatchLogs_Role
  - id: AROAITPA5S7NF4PREISDM
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "cloudtrail.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


CrossAccountSign:
  aws.iam.role.present:
  - resource_id: CrossAccountSign
  - name: CrossAccountSign
  - arn: arn:aws:iam::123456789012:role/CrossAccountSign
  - id: AROAIBW7XSQDB6LTDCF6K
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::565541806941:root"}}],
      "Version": "2012-10-17"}'


Executelambda:
  aws.iam.role.present:
  - resource_id: Executelambda
  - name: Executelambda
  - arn: arn:aws:iam::123456789012:role/Executelambda
  - id: AROAIYDNOY3U7VXI3ZMSG
  - path: /
  - description: Allows Lambda functions to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


Fori18ntest_+=,.@-:
  aws.iam.role.present:
  - resource_id: Fori18ntest_+=,.@-
  - name: Fori18ntest_+=,.@-
  - arn: arn:aws:iam::123456789012:role/Fori18ntest_+=,.@-
  - id: AROAILCLFKW7MRBX5LTPI
  - path: /
  - description: Fori18ntest_+=,.@-
  - max_session_duration: 3600
  - tags:
    - Key: Fori18ntest _.:/=+-@
      Value: Fori18ntest _.:/=+-@
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "bd57bfa55a312f557abc545ad12a94e3b456ca7bbf3a3b0c71cbcf13b23c13ec"}},
      "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::993194883101:root"}}],
      "Version": "2012-10-17"}'


Idem_iam_vj_test:
  aws.iam.role.present:
  - resource_id: Idem_iam_vj_test
  - name: Idem_iam_vj_test
  - arn: arn:aws:iam::123456789012:role/Idem_iam_vj_test
  - id: AROAX2FJ77DCZLZEGFD6D
  - path: /
  - description: Allows access to other AWS service resources that are required to
      operate clusters managed by xyz.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}, {"Action": "sts:AssumeRole",
      "Effect": "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version":
      "2012-10-17"}'


KK+=HAHA:
  aws.iam.role.present:
  - resource_id: KK+=HAHA
  - name: KK+=HAHA
  - arn: arn:aws:iam::123456789012:role/KK+=HAHA
  - id: AROAIX3RRKRKSQWD4MC7O
  - path: /
  - description: KK DE LINGDE
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::993194883101:root"}}],
      "Version": "2012-10-17"}'


MyAppAdmin:
  aws.iam.role.present:
  - resource_id: MyAppAdmin
  - name: MyAppAdmin
  - arn: arn:aws:iam::123456789012:role/service-role/MyAppAdmin
  - id: AROAX2FJ77DCQB45AGYRJ
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


ReadOnlyEC2:
  aws.iam.role.present:
  - resource_id: ReadOnlyEC2
  - name: ReadOnlyEC2
  - arn: arn:aws:iam::123456789012:role/ReadOnlyEC2
  - id: AROAIXXNNOS7CQ6EFBDSO
  - path: /
  - description: Used for testing purposes.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"sts:ExternalId": ["prelude_test_external_id",
      "permissionstest"]}}, "Effect": "Allow", "Principal": {"AWS": ["AIDAX2FJ77DCWZQKD5MOL",
      "arn:aws:iam::746014882121:root"]}}], "Version": "2012-10-17"}'


S3BucketAccess:
  aws.iam.role.present:
  - resource_id: S3BucketAccess
  - name: S3BucketAccess
  - arn: arn:aws:iam::123456789012:role/S3BucketAccess
  - id: AROAX2FJ77DCSWZPLWBKN
  - path: /
  - description: Allows EC2 instances to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


SecureState:
  aws.iam.role.present:
  - resource_id: SecureState
  - name: SecureState
  - arn: arn:aws:iam::123456789012:role/SecureState
  - id: AROAX2FJ77DC4JNHFZVT3
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "pWvdXbAberZWiAjlBowqJyhSvgzlJxjv"}}, "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::530342348278:root"}}], "Version":
      "2012-10-17"}'


SecureStateRole:
  aws.iam.role.present:
  - resource_id: SecureStateRole
  - name: SecureStateRole
  - arn: arn:aws:iam::123456789012:role/SecureStateRole
  - id: AROAX2FJ77DCRG33VYAJA
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::993194883101:root"}}],
      "Version": "2012-10-17"}'


StatesExecutionRole:
  aws.iam.role.present:
  - resource_id: StatesExecutionRole
  - name: StatesExecutionRole
  - arn: arn:aws:iam::123456789012:role/service-role/StatesExecutionRole
  - id: AROAJUP7YTEZSKB3AX4LW
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "states.amazonaws.com"}}], "Version": "2012-10-17"}'


VMWMasterReadOnlyRole:
  aws.iam.role.present:
  - resource_id: VMWMasterReadOnlyRole
  - name: VMWMasterReadOnlyRole
  - arn: arn:aws:iam::123456789012:role/VMWMasterReadOnlyRole
  - id: AROAX2FJ77DCRWKPLXQCL
  - path: /
  - description: Read Only Role for the Master Payer Account
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::116462199383:root"}}], "Version":
      "2012-10-17"}'


VMWMasterRole:
  aws.iam.role.present:
  - resource_id: VMWMasterRole
  - name: VMWMasterRole
  - arn: arn:aws:iam::123456789012:role/VMWMasterRole
  - id: AROAJ5RG24UTE63G4RT56
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::116462199383:root"}}], "Version":
      "2012-10-17"}'


Wavefront:
  aws.iam.role.present:
  - resource_id: Wavefront
  - name: Wavefront
  - arn: arn:aws:iam::123456789012:role/Wavefront
  - id: AROAJTKVFVMV32QSDVUOW
  - path: /
  - description: Wavefront
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "waveb24f6af3"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::301213811993:root"}}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I
  - name: abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I
  - id: AROAX2FJ77DCYXGCA5DNA
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::347624956669:root"}}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - name: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - id: AROAX2FJ77DC3KEAINRQT
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL
  - name: abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL
  - id: AROAX2FJ77DC56PYYNOTN
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::909992878262:root"}}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW
  - name: abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW
  - id: AROAX2FJ77DCT7ZABH5WM
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::083910886865:root"}}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX
  - name: abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX
  - id: AROAX2FJ77DC4AALNKS5P
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::347624956669:root"}}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - name: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - id: AROAX2FJ77DCSVRRFFAO2
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T
  - name: abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T
  - id: AROAX2FJ77DCRMYV7SWVZ
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::909992878262:root"}}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8
  - name: abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8
  - id: AROAX2FJ77DC4SEMMXRG7
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::557453615640:root"}}], "Version": "2012-10-17"}'


afilipov:
  aws.iam.role.present:
  - resource_id: afilipov
  - name: afilipov
  - arn: arn:aws:iam::123456789012:role/afilipov
  - id: AROAX2FJ77DC3KYL746XR
  - path: /
  - description: Allows Lambda functions to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


apigateway-sqs-access-role:
  aws.iam.role.present:
  - resource_id: apigateway-sqs-access-role
  - name: apigateway-sqs-access-role
  - arn: arn:aws:iam::123456789012:role/apigateway-sqs-access-role
  - id: AROAIYBMR7BDONKSJ4OWY
  - path: /
  - description: Allows API Gateway to monitor azure events.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "apigateway.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


aws-ec2-spot-fleet-autoscale-role:
  aws.iam.role.present:
  - resource_id: aws-ec2-spot-fleet-autoscale-role
  - name: aws-ec2-spot-fleet-autoscale-role
  - arn: arn:aws:iam::123456789012:role/aws-ec2-spot-fleet-autoscale-role
  - id: AROAIFJXKTO4N4L4IDXUI
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "application-autoscaling.amazonaws.com"},
      "Sid": ""}], "Version": "2012-10-17"}'


aws-ec2-spot-fleet-tagging-role:
  aws.iam.role.present:
  - resource_id: aws-ec2-spot-fleet-tagging-role
  - name: aws-ec2-spot-fleet-tagging-role
  - arn: arn:aws:iam::123456789012:role/aws-ec2-spot-fleet-tagging-role
  - id: AROAJ4GSDAXW637ILXHWA
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spotfleet.amazonaws.com"}, "Sid": ""}], "Version":
      "2012-10-17"}'


aws-elasticbeanstalk-ec2-role:
  aws.iam.role.present:
  - resource_id: aws-elasticbeanstalk-ec2-role
  - name: aws-elasticbeanstalk-ec2-role
  - arn: arn:aws:iam::123456789012:role/aws-elasticbeanstalk-ec2-role
  - id: AROAX2FJ77DC5DELYTJZV
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2008-10-17"}'


aws-elasticbeanstalk-service-role:
  aws.iam.role.present:
  - resource_id: aws-elasticbeanstalk-service-role
  - name: aws-elasticbeanstalk-service-role
  - arn: arn:aws:iam::123456789012:role/aws-elasticbeanstalk-service-role
  - id: AROAX2FJ77DCUFJOVFA6A
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "elasticbeanstalk"}}, "Effect": "Allow",
      "Principal": {"Service": "elasticbeanstalk.amazonaws.com"}}], "Version": "2012-10-17"}'


aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole
  - name: aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCQ7JZUN46Y
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


bn0lunfd-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: bn0lunfd-dev-us-east-1-lambdaRole
  - name: bn0lunfd-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/bn0lunfd-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCXDNF35CEQ
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


cluster01-InfosecVulnScanRole:
  aws.iam.role.present:
  - resource_id: cluster01-InfosecVulnScanRole
  - name: cluster01-InfosecVulnScanRole
  - arn: arn:aws:iam::123456789012:role/cluster01-InfosecVulnScanRole
  - id: AROAX2FJ77DC7HX4KPKQB
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}, "Sid": ""}], "Version":
      "2012-10-17"}'


cluster01-temp-xyz:
  aws.iam.role.present:
  - resource_id: cluster01-temp-xyz
  - name: cluster01-temp-xyz
  - arn: arn:aws:iam::123456789012:role/cluster01-temp-xyz
  - id: AROAX2FJ77DCXP6NZEXRE
  - path: /
  - description: Allows EC2 instances to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


cluster01-temp-xyz-cluster-node:
  aws.iam.role.present:
  - resource_id: cluster01-temp-xyz-cluster-node
  - name: cluster01-temp-xyz-cluster-node
  - arn: arn:aws:iam::123456789012:role/cluster01-temp-xyz-cluster-node
  - id: AROAX2FJ77DC633PDXM3K
  - path: /
  - description: Allows EC2 instances to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


clusterlifecycle.tmc.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: clusterlifecycle.tmc.cloud.abc.com
  - name: clusterlifecycle.tmc.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/clusterlifecycle.tmc.cloud.abc.com
  - id: AROAX2FJ77DCRUSPQE5PS
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "342fa94e-7f90-5f08-b459-1b0479e9b7cc"}},
      "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::630260974543:role/whitesand-aws-usw2-mgmt-Kiam-Server-Role"}}],
      "Version": "2012-10-17"}'


config-role-us-east-1:
  aws.iam.role.present:
  - resource_id: config-role-us-east-1
  - name: config-role-us-east-1
  - arn: arn:aws:iam::123456789012:role/service-role/config-role-us-east-1
  - id: AROAJXGXUGHLEHWNVOOSY
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "config.amazonaws.com"}, "Sid": ""}], "Version":
      "2012-10-17"}'


control-plane.tkg.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: control-plane.tkg.cloud.abc.com
  - name: control-plane.tkg.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/control-plane.tkg.cloud.abc.com
  - id: AROAX2FJ77DCTKKPSKXZF
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


control-plane.tmc.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: control-plane.tmc.cloud.abc.com
  - name: control-plane.tmc.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/control-plane.tmc.cloud.abc.com
  - id: AROAX2FJ77DC6V5TZK7SS
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


controllers.tkg.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: controllers.tkg.cloud.abc.com
  - name: controllers.tkg.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/controllers.tkg.cloud.abc.com
  - id: AROAX2FJ77DCW3RYJFY5C
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


default-test-temp-xyz-cluster:
  aws.iam.role.present:
  - resource_id: default-test-temp-xyz-cluster
  - name: default-test-temp-xyz-cluster
  - arn: arn:aws:iam::123456789012:role/default-test-temp-xyz-cluster
  - id: AROAX2FJ77DC6SU5DTHYB
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Environment
      Value: default
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


ecsInstanceRole:
  aws.iam.role.present:
  - resource_id: ecsInstanceRole
  - name: ecsInstanceRole
  - arn: arn:aws:iam::123456789012:role/ecsInstanceRole
  - id: AROAI6QGH7NNYYR5TDIOC
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}, "Sid": ""}], "Version":
      "2008-10-17"}'


ecsTaskExecutionRole:
  aws.iam.role.present:
  - resource_id: ecsTaskExecutionRole
  - name: ecsTaskExecutionRole
  - arn: arn:aws:iam::123456789012:role/ecsTaskExecutionRole
  - id: AROAX2FJ77DCXK7IXAFHG
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ecs-tasks.amazonaws.com"}, "Sid": ""}], "Version":
      "2008-10-17"}'


f2scv1hr-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: f2scv1hr-dev-us-east-1-lambdaRole
  - name: f2scv1hr-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/f2scv1hr-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCUAPA7PNZX
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


go-test-role-4uc6dizk:
  aws.iam.role.present:
  - resource_id: go-test-role-4uc6dizk
  - name: go-test-role-4uc6dizk
  - arn: arn:aws:iam::123456789012:role/service-role/go-test-role-4uc6dizk
  - id: AROAX2FJ77DCUCB3LKFE3
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


go-test-role-9mmf25d1:
  aws.iam.role.present:
  - resource_id: go-test-role-9mmf25d1
  - name: go-test-role-9mmf25d1
  - arn: arn:aws:iam::123456789012:role/service-role/go-test-role-9mmf25d1
  - id: AROAX2FJ77DC3X6MKSX3E
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


guardduty-event-role:
  aws.iam.role.present:
  - resource_id: guardduty-event-role
  - name: guardduty-event-role
  - arn: arn:aws:iam::123456789012:role/guardduty-event-role
  - id: AROAX2FJ77DCXSLVF6ATC
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: ManagedBy
      Value: ConformityEngine
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "events.amazonaws.com"}}], "Version": "2012-10-17"}'


hello-world-serverless-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: hello-world-serverless-dev-us-east-1-lambdaRole
  - name: hello-world-serverless-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/hello-world-serverless-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DC75KMSK5OR
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472:
  aws.iam.role.present:
  - resource_id: idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - name: idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - arn: arn:aws:iam::123456789012:role/idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - id: AROAX2FJ77DCS2KUCRY2U
  - path: /
  - description: Idem IAM role integration test fixture
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}, {"Action": "sts:AssumeRole",
      "Effect": "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version":
      "2012-10-17"}'


idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80:
  aws.iam.role.present:
  - resource_id: idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - name: idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - arn: arn:aws:iam::123456789012:role/idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - id: AROAX2FJ77DC4UR543KD4
  - path: /
  - description: Idem IAM role integration test fixture
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}, {"Action": "sts:AssumeRole",
      "Effect": "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version":
      "2012-10-17"}'


idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4:
  aws.iam.role.present:
  - resource_id: idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - name: idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - arn: arn:aws:iam::123456789012:role/idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - id: AROAX2FJ77DCQI6G2X55L
  - path: /
  - description: Idem IAM role integration test fixture
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}, {"Action": "sts:AssumeRole",
      "Effect": "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version":
      "2012-10-17"}'


idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9:
  aws.iam.role.present:
  - resource_id: idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9
  - name: idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9
  - arn: arn:aws:iam::123456789012:role/idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9
  - id: AROAX2FJ77DCR4EKXW32Q
  - path: /
  - description: Idem IAM role integration test
  - max_session_duration: 3700
  - tags:
    - Key: Name
      Value: idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478:
  aws.iam.role.present:
  - resource_id: idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478
  - name: idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478
  - arn: arn:aws:iam::123456789012:role/idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478
  - id: AROAX2FJ77DC5BZ3LC232
  - path: /
  - description: Idem IAM role integration test
  - max_session_duration: 3700
  - tags:
    - Key: Name
      Value: idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543:
  aws.iam.role.present:
  - resource_id: idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543
  - name: idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543
  - arn: arn:aws:iam::123456789012:role/idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543
  - id: AROAX2FJ77DCSALRGM4OP
  - path: /
  - description: Idem IAM role test description updated
  - max_session_duration: 3800
  - tags:
    - Key: idem-test-iam-key-048b3bc6-1e7e-4ed4-b599-d570c8ba2579
      Value: idem-test-iam-value-fad4ba66-2e1a-4257-87a8-88ffe8e96edd
    - Key: Name
      Value: idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster:
  aws.iam.role.present:
  - resource_id: idem-test-temp-xyz-cluster
  - name: idem-test-temp-xyz-cluster
  - arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster
  - id: AROAX2FJ77DC2JM67OZSY
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster-node:
  aws.iam.role.present:
  - resource_id: idem-test-temp-xyz-cluster-node
  - name: idem-test-temp-xyz-cluster-node
  - arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster-node
  - id: AROAX2FJ77DC6POVG7T5L
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


java-test-role-wex7ne6b:
  aws.iam.role.present:
  - resource_id: java-test-role-wex7ne6b
  - name: java-test-role-wex7ne6b
  - arn: arn:aws:iam::123456789012:role/service-role/java-test-role-wex7ne6b
  - id: AROAX2FJ77DC5A2VOUCLT
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


ki5roed2-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: ki5roed2-dev-us-east-1-lambdaRole
  - name: ki5roed2-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/ki5roed2-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCXK5X2PMKC
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


krisi-temp:
  aws.iam.role.present:
  - resource_id: krisi-temp
  - name: krisi-temp
  - arn: arn:aws:iam::123456789012:role/krisi-temp
  - id: AROAX2FJ77DC6KLXMCMPY
  - path: /
  - description: Allow obtaining and decrypting parameters in the user-data of an
      instance
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


lambdaExecutionRole:
  aws.iam.role.present:
  - resource_id: lambdaExecutionRole
  - name: lambdaExecutionRole
  - arn: arn:aws:iam::123456789012:role/service-role/lambdaExecutionRole
  - id: AROAIBJ5EOBFRV5GYFGC4
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


masters.training.k8s.local:
  aws.iam.role.present:
  - resource_id: masters.training.k8s.local
  - name: masters.training.k8s.local
  - arn: arn:aws:iam::123456789012:role/masters.training.k8s.local
  - id: AROAJNJFDWDHVLENZXBSK
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


migrationhub-discovery:
  aws.iam.role.present:
  - resource_id: migrationhub-discovery
  - name: migrationhub-discovery
  - arn: arn:aws:iam::123456789012:role/service-role/migrationhub-discovery
  - id: AROAX2FJ77DCTS5IBOACY
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "migrationhub.amazonaws.com"}}], "Version":
      "2012-10-17"}'


my-s3-function-role:
  aws.iam.role.present:
  - resource_id: my-s3-function-role
  - name: my-s3-function-role
  - arn: arn:aws:iam::123456789012:role/service-role/my-s3-function-role
  - id: AROAX2FJ77DCXJ6MMTVQC
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


myFirstAPI-role-py7jxvn3:
  aws.iam.role.present:
  - resource_id: myFirstAPI-role-py7jxvn3
  - name: myFirstAPI-role-py7jxvn3
  - arn: arn:aws:iam::123456789012:role/service-role/myFirstAPI-role-py7jxvn3
  - id: AROAX2FJ77DCXBLUKYW2K
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


myTEST-role-ovnuxvqo:
  aws.iam.role.present:
  - resource_id: myTEST-role-ovnuxvqo
  - name: myTEST-role-ovnuxvqo
  - arn: arn:aws:iam::123456789012:role/service-role/myTEST-role-ovnuxvqo
  - id: AROAX2FJ77DCTPHMDE345
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


my_new_role:
  aws.iam.role.present:
  - resource_id: my_new_role
  - name: my_new_role
  - arn: arn:aws:iam::123456789012:role/my_new_role
  - id: AROAJJGC2M32V3S7ULRFA
  - path: /
  - description: Allows EC2 instances to call AWS services on your behalf.
  - max_session_duration: 3600
  - tags:
    - Key: test-1
      Value: test-val-1
    - Key: test-2
      Value: test-val-2
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spotfleet.amazonaws.com"}}], "Version": "2012-10-17"}'


nodes.tkg.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: nodes.tkg.cloud.abc.com
  - name: nodes.tkg.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/nodes.tkg.cloud.abc.com
  - id: AROAX2FJ77DC75PUXUSTB
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


nodes.tmc.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: nodes.tmc.cloud.abc.com
  - name: nodes.tmc.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/nodes.tmc.cloud.abc.com
  - id: AROAX2FJ77DC6H2KFTNNS
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


nodes.training.k8s.local:
  aws.iam.role.present:
  - resource_id: nodes.training.k8s.local
  - name: nodes.training.k8s.local
  - arn: arn:aws:iam::123456789012:role/nodes.training.k8s.local
  - id: AROAJQCCTG247P2YJ3Y5U
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


org1_cluster01_potato_dev_k8s_admins:
  aws.iam.role.present:
  - resource_id: org1_cluster01_potato_dev_k8s_admins
  - name: org1_cluster01_potato_dev_k8s_admins
  - arn: arn:aws:iam::123456789012:role/org1_cluster01_potato_dev_k8s_admins
  - id: AROAX2FJ77DCXVB3S3YEU
  - path: /
  - max_session_duration: 43200
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_cluster01_potato_dev_k8s_admins"}}, "Effect": "Allow", "Principal": {"Federated":
      "arn:aws:iam::123456789012:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_cluster01_potato_dev_k8s_readonly:
  aws.iam.role.present:
  - resource_id: org1_cluster01_potato_dev_k8s_readonly
  - name: org1_cluster01_potato_dev_k8s_readonly
  - arn: arn:aws:iam::123456789012:role/org1_cluster01_potato_dev_k8s_readonly
  - id: AROAX2FJ77DCTTZZ6XA5T
  - path: /
  - max_session_duration: 43200
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_cluster01_potato_dev_k8s_readonly"}}, "Effect": "Allow", "Principal":
      {"Federated": "arn:aws:iam::123456789012:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_idem-test_vidmGroup_all_profile_k8s_admins:
  aws.iam.role.present:
  - resource_id: org1_idem-test_vidmGroup_all_profile_k8s_admins
  - name: org1_idem-test_vidmGroup_all_profile_k8s_admins
  - arn: arn:aws:iam::123456789012:role/org1_idem-test_vidmGroup_all_profile_k8s_admins
  - id: AROAX2FJ77DCQMVVOYGPC
  - path: /
  - max_session_duration: 43200
  - tags:
    - Key: Environment
      Value: default
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_idem-test_vidmGroup_all_profile_k8s_admins"}}, "Effect": "Allow", "Principal":
      {"Federated": "arn:aws:iam::my_account:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_idem_test_potato_dev_k8s_admins:
  aws.iam.role.present:
  - resource_id: org1_idem_test_potato_dev_k8s_admins
  - name: org1_idem_test_potato_dev_k8s_admins
  - arn: arn:aws:iam::123456789012:role/org1_idem_test_potato_dev_k8s_admins
  - id: AROAX2FJ77DCRFA2KD4FU
  - path: /
  - max_session_duration: 43200
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_idem_test_potato_dev_k8s_admins"}}, "Effect": "Allow", "Principal": {"Federated":
      "arn:aws:iam::123456789012:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_idem_test_potato_dev_k8s_readonly:
  aws.iam.role.present:
  - resource_id: org1_idem_test_potato_dev_k8s_readonly
  - name: org1_idem_test_potato_dev_k8s_readonly
  - arn: arn:aws:iam::123456789012:role/org1_idem_test_potato_dev_k8s_readonly
  - id: AROAX2FJ77DC4TGQSSUEE
  - path: /
  - max_session_duration: 43200
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_idem_test_potato_dev_k8s_readonly"}}, "Effect": "Allow", "Principal":
      {"Federated": "arn:aws:iam::123456789012:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


powershell-role-gj7p5czv:
  aws.iam.role.present:
  - resource_id: powershell-role-gj7p5czv
  - name: powershell-role-gj7p5czv
  - arn: arn:aws:iam::123456789012:role/service-role/powershell-role-gj7p5czv
  - id: AROAX2FJ77DCT3WUMYDBS
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


pvbhullg-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: pvbhullg-dev-us-east-1-lambdaRole
  - name: pvbhullg-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/pvbhullg-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCZVPSKQDVX
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


rds-monitoring-role:
  aws.iam.role.present:
  - resource_id: rds-monitoring-role
  - name: rds-monitoring-role
  - arn: arn:aws:iam::123456789012:role/rds-monitoring-role
  - id: AROAJ2YIUIG6C2RGSUPCW
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "monitoring.rds.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


secure-state-event-role:
  aws.iam.role.present:
  - resource_id: secure-state-event-role
  - name: secure-state-event-role
  - arn: arn:aws:iam::123456789012:role/secure-state-event-role
  - id: AROAX2FJ77DCUMXWHC356
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: ManagedBy
      Value: ConformityEngine
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "events.amazonaws.com"}}], "Version": "2012-10-17"}'


securestate-role:
  aws.iam.role.present:
  - resource_id: securestate-role
  - name: securestate-role
  - arn: arn:aws:iam::123456789012:role/securestate-role
  - id: AROAX2FJ77DCR337N3O2J
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: ManagedBy
      Value: ConformityEngine
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "yC7ArT3F57MC8QHlVagibYz9YMce"}}, "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::910887748405:root"}}], "Version":
      "2012-10-17"}'


serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY:
  aws.iam.role.present:
  - resource_id: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - name: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - arn: arn:aws:iam::123456789012:role/serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - id: AROAX2FJ77DCUDCV5ZNCI
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: serverlessrepo:semanticVersion
      Value: 1.0.4
    - Key: lambda:createdBy
      Value: SAM
    - Key: serverlessrepo:applicationId
      Value: arn:aws:serverlessrepo:us-east-1:077246666028:applications/hello-world
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


service-09vpvytp-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: service-09vpvytp-dev-us-east-1-lambdaRole
  - name: service-09vpvytp-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/service-09vpvytp-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DC5NIE5QHPY
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK:
  aws.iam.role.present:
  - resource_id: spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - name: spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - arn: arn:aws:iam::123456789012:role/spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - id: AROAX2FJ77DC5CXAUUMSQ
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "5iOE4qbqkHcyajclWh9AXm-YhZWa5t7Qo27CFhfS7Js-"}},
      "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::922761411349:root"}}],
      "Version": "2012-10-17"}'


spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG:
  aws.iam.role.present:
  - resource_id: spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - name: spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - arn: arn:aws:iam::123456789012:role/spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - id: AROAX2FJ77DCSPDY5NPT2
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "i7IqcLtbB11zzsfahn3lLP8hLguVmGV1jzKrwFmv6eA-"}},
      "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::922761411349:root"}}],
      "Version": "2012-10-17"}'


t1-role-cn88ujwv:
  aws.iam.role.present:
  - resource_id: t1-role-cn88ujwv
  - name: t1-role-cn88ujwv
  - arn: arn:aws:iam::123456789012:role/service-role/t1-role-cn88ujwv
  - id: AROAX2FJ77DCXRVYLLRYE
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


t2guydv5-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: t2guydv5-dev-us-east-1-lambdaRole
  - name: t2guydv5-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/t2guydv5-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCTAHNNN72O
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


test:
  aws.iam.role.present:
  - resource_id: test
  - name: test
  - arn: arn:aws:iam::123456789012:role/service-role/test
  - id: AROAX2FJ77DCZ632NHVVI
  - path: /service-role/
  - description: Proton pipeline service role
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "proton.amazonaws.com"}}], "Version": "2012-10-17"}'


test-xyz_fargate:
  aws.iam.role.present:
  - resource_id: test-xyz_fargate
  - name: test-xyz_fargate
  - arn: arn:aws:iam::123456789012:role/test-xyz_fargate
  - id: AROAX2FJ77DCUZU4M6BP7
  - path: /
  - description: Allows access to other AWS service resources that are required to
      run Amazon xyz pods on AWS Fargate.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz-fargate-pods.amazonaws.com"}}], "Version":
      "2012-10-17"}'


tgeorgiev-lambda-dynamodb:
  aws.iam.role.present:
  - resource_id: tgeorgiev-lambda-dynamodb
  - name: tgeorgiev-lambda-dynamodb
  - arn: arn:aws:iam::123456789012:role/tgeorgiev-lambda-dynamodb
  - id: AROAX2FJ77DC3KAWRBNJ7
  - path: /
  - description: Allows Lambda functions to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


vieworgspolicies:
  aws.iam.role.present:
  - resource_id: vieworgspolicies
  - name: vieworgspolicies
  - arn: arn:aws:iam::123456789012:role/vieworgspolicies
  - id: AROAX2FJ77DCZRY2OJLXC
  - path: /
  - description: vieworgspolicies
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}}],
      "Version": "2012-10-17"}'


vmw-cloudhealth-role:
  aws.iam.role.present:
  - resource_id: vmw-cloudhealth-role
  - name: vmw-cloudhealth-role
  - arn: arn:aws:iam::123456789012:role/vmw-cloudhealth-role
  - id: AROAJDM6SWVHL3ZLVIJBU
  - path: /
  - description: Role for AWS-CloudHealth integration at abc
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "81320172017"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::454464851268:root"}}], "Version": "2012-10-17"}'


vmws-config-role:
  aws.iam.role.present:
  - resource_id: vmws-config-role
  - name: vmws-config-role
  - arn: arn:aws:iam::123456789012:role/vmws-config-role
  - id: AROAX2FJ77DCZW4QMKR7K
  - path: /
  - description: Role for abc AWS Config - Do not delete
  - max_session_duration: 3600
  - tags:
    - Key: ManagedBy
      Value: ConformityEngine
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "config.amazonaws.com"}, "Sid": ""}], "Version":
      "2012-10-17"}'


xyz-cluster01-admin:
  aws.iam.role.present:
  - resource_id: xyz-cluster01-admin
  - name: xyz-cluster01-admin
  - arn: arn:aws:iam::123456789012:role/xyz-cluster01-admin
  - id: AROAX2FJ77DCS4HYWPSRF
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Environment
      Value: default
    - Key: KubernetesCluster
      Value: cluster01
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"aws:username": ["user1", "user2", "user3", "user4",
      "user5"]}}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}},
      {"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:role/xyz-cluster01-admin"}}],
      "Version": "2012-10-17"}'


xyz-cluster01_redlock_flow_role:
  aws.iam.role.present:
  - resource_id: xyz-cluster01_redlock_flow_role
  - name: xyz-cluster01_redlock_flow_role
  - arn: arn:aws:iam::123456789012:role/xyz-cluster01_redlock_flow_role
  - id: AROAX2FJ77DCQTHAPMDNN
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: cluster01
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "vpc-flow-logs.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


xyz-cluster02-fargate-pod-execution-role:
  aws.iam.role.present:
  - resource_id: xyz-cluster02-fargate-pod-execution-role
  - name: xyz-cluster02-fargate-pod-execution-role
  - arn: arn:aws:iam::123456789012:role/xyz-cluster02-fargate-pod-execution-role
  - id: AROAX2FJ77DCXS2XYYNNQ
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": ["xyz-fargate-pods.amazonaws.com", "xyz.amazonaws.com"]}}],
      "Version": "2012-10-17"}'


xyz-idem-test-admin:
  aws.iam.role.present:
  - resource_id: xyz-idem-test-admin
  - name: xyz-idem-test-admin
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-admin
  - id: AROAX2FJ77DC33OWDECR6
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"aws:username": ["user1", "user2", "user3", "user4",
      "user5"]}}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}},
      {"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}],
      "Version": "2012-10-17"}'


xyz-idem-test-fargate-pod-execution-role:
  aws.iam.role.present:
  - resource_id: xyz-idem-test-fargate-pod-execution-role
  - name: xyz-idem-test-fargate-pod-execution-role
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-fargate-pod-execution-role
  - id: AROAX2FJ77DC6EXFK3CYE
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": ["xyz.amazonaws.com", "xyz-fargate-pods.amazonaws.com"]}}],
      "Version": "2012-10-17"}'


xyz-idem-test-jenkins:
  aws.iam.role.present:
  - resource_id: xyz-idem-test-jenkins
  - name: xyz-idem-test-jenkins
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-jenkins
  - id: AROAX2FJ77DCQHIULDKIS
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"aws:username": ["user1", "user2", "user3", "user4",
      "user5"]}}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}},
      {"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:user/xyz/idem-test/extension-jenkins-idem-test"}}],
      "Version": "2012-10-17"}'


xyz-idem-test_redlock_flow_role:
  aws.iam.role.present:
  - resource_id: xyz-idem-test_redlock_flow_role
  - name: xyz-idem-test_redlock_flow_role
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - id: AROAX2FJ77DC7NUDJJ6DH
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "vpc-flow-logs.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


xyzClusterRole:
  aws.iam.role.present:
  - resource_id: xyzClusterRole
  - name: xyzClusterRole
  - arn: arn:aws:iam::123456789012:role/xyzClusterRole
  - id: AROAX2FJ77DCUPA62WEVL
  - path: /
  - description: Allows access to other AWS service resources that are required to
      operate clusters managed by xyz.
  - max_session_duration: 14400
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzManageRole:
  aws.iam.role.present:
  - resource_id: xyzManageRole
  - name: xyzManageRole
  - arn: arn:aws:iam::123456789012:role/xyzManageRole
  - id: AROAI6URWWBKJCLNVOAW2
  - path: /
  - description: Allows xyz to manage clusters on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzcluster:
  aws.iam.role.present:
  - resource_id: xyzcluster
  - name: xyzcluster
  - arn: arn:aws:iam::123456789012:role/xyzcluster
  - id: AROAX2FJ77DCWNFKJVIOQ
  - path: /
  - description: Allows access to other AWS service resources that are required to
      operate clusters managed by xyz.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2:
  aws.iam.role.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2
  - name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2
  - arn: arn:aws:iam::123456789012:role/xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2
  - id: AROAX2FJ77DCYKAQDLEQU
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: alpha.xyzctl.io/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: xyzctl.cluster.k8s.io/v1alpha1/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: alpha.xyzctl.io/iamserviceaccount-name
      Value: kube-system/alb-ingress-controller
    - Key: alpha.xyzctl.io/xyzctl-version
      Value: 0.77.0
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"StringEquals": {"oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:aud":
      "sts.amazonaws.com", "oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:sub":
      "system:serviceaccount:kube-system:alb-ingress-controller"}}, "Effect": "Allow",
      "Principal": {"Federated": "arn:aws:iam::123456789012:oidc-provider/oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A"}}],
      "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0:
  aws.iam.role.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - arn: arn:aws:iam::123456789012:role/xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - id: AROAX2FJ77DCWVCEYIHTX
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: alpha.xyzctl.io/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: xyzctl.cluster.k8s.io/v1alpha1/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: alpha.xyzctl.io/iamserviceaccount-name
      Value: kube-system/aws-load-balancer-controller
    - Key: alpha.xyzctl.io/xyzctl-version
      Value: 0.77.0
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"StringEquals": {"oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:aud":
      "sts.amazonaws.com", "oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:sub":
      "system:serviceaccount:kube-system:aws-load-balancer-controller"}}, "Effect":
      "Allow", "Principal": {"Federated": "arn:aws:iam::123456789012:oidc-provider/oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A"}}],
      "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY:
  aws.iam.role.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - arn: arn:aws:iam::123456789012:role/xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - id: AROAX2FJ77DCZQCWEQHHQ
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: alpha.xyzctl.io/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: xyzctl.cluster.k8s.io/v1alpha1/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: alpha.xyzctl.io/xyzctl-version
      Value: 0.77.0
    - Key: Name
      Value: xyzctl-pr-ssc-xyz-poc-cluster/ServiceRole
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50:
  aws.iam.role.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - arn: arn:aws:iam::123456789012:role/xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - id: AROAX2FJ77DCYLCUYMEWE
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: alpha.xyzctl.io/nodegroup-name
      Value: ng-027a0eb6
    - Key: alpha.xyzctl.io/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: xyzctl.cluster.k8s.io/v1alpha1/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: alpha.xyzctl.io/nodegroup-type
      Value: managed
    - Key: alpha.xyzctl.io/xyzctl-version
      Value: 0.77.0
    - Key: Name
      Value: xyzctl-pr-ssc-xyz-poc-nodegroup-ng-027a0eb6/NodeInstanceRole
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzfargateprofile-role:
  aws.iam.role.present:
  - resource_id: xyzfargateprofile-role
  - name: xyzfargateprofile-role
  - arn: arn:aws:iam::123456789012:role/xyzfargateprofile-role
  - id: AROAX2FJ77DC2FQOJAREM
  - path: /
  - description: Allows access to other AWS service resources that are required to
      run Amazon xyz pods on AWS Fargate.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz-fargate-pods.amazonaws.com"}}], "Version":
      "2012-10-17"}'
