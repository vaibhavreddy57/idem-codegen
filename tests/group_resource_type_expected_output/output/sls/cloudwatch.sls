/aws/lambda/31a8ef8a50736cea29cce7afbb9856dd559433bae450bf3d55dfc372:
  aws.cloudwatch.log_group.present:
  - name: /aws/lambda/31a8ef8a50736cea29cce7afbb9856dd559433bae450bf3d55dfc372
  - resource_id: /aws/lambda/31a8ef8a50736cea29cce7afbb9856dd559433bae450bf3d55dfc372
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/lambda/31a8ef8a50736cea29cce7afbb9856dd559433bae450bf3d55dfc372:*


/aws/lambda/6ddae39149a1caffbede059ffb24fd2259f8f39d10846f613bc48675:
  aws.cloudwatch.log_group.present:
  - name: /aws/lambda/6ddae39149a1caffbede059ffb24fd2259f8f39d10846f613bc48675
  - resource_id: /aws/lambda/6ddae39149a1caffbede059ffb24fd2259f8f39d10846f613bc48675
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/lambda/6ddae39149a1caffbede059ffb24fd2259f8f39d10846f613bc48675:*


/aws/lambda/8f84e01cc0193b496cc472e99e3d5f6d809b6a60e3ed89002d9eda31:
  aws.cloudwatch.log_group.present:
  - name: /aws/lambda/8f84e01cc0193b496cc472e99e3d5f6d809b6a60e3ed89002d9eda31
  - resource_id: /aws/lambda/8f84e01cc0193b496cc472e99e3d5f6d809b6a60e3ed89002d9eda31
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/lambda/8f84e01cc0193b496cc472e99e3d5f6d809b6a60e3ed89002d9eda31:*


/aws/lambda/aa71f3f1ab19dced17d2b99a55dd3708dc121bb127e95188069c87e9:
  aws.cloudwatch.log_group.present:
  - name: /aws/lambda/aa71f3f1ab19dced17d2b99a55dd3708dc121bb127e95188069c87e9
  - resource_id: /aws/lambda/aa71f3f1ab19dced17d2b99a55dd3708dc121bb127e95188069c87e9
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/lambda/aa71f3f1ab19dced17d2b99a55dd3708dc121bb127e95188069c87e9:*


/aws/xyz/idem-cluster-1/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-cluster-1/cluster
  - resource_id: /aws/xyz/idem-cluster-1/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-cluster-1/cluster:*


/aws/xyz/idem-test-cluster-01963890-b18d-4a2f-9fee-bf544342c565/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-01963890-b18d-4a2f-9fee-bf544342c565/cluster
  - resource_id: /aws/xyz/idem-test-cluster-01963890-b18d-4a2f-9fee-bf544342c565/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-01963890-b18d-4a2f-9fee-bf544342c565/cluster:*


/aws/xyz/idem-test-cluster-0f764b68-04a0-4775-8f79-c096311c9740/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-0f764b68-04a0-4775-8f79-c096311c9740/cluster
  - resource_id: /aws/xyz/idem-test-cluster-0f764b68-04a0-4775-8f79-c096311c9740/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-0f764b68-04a0-4775-8f79-c096311c9740/cluster:*


/aws/xyz/idem-test-cluster-11d35ba3-68fe-436a-a774-e9c1c415f424/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-11d35ba3-68fe-436a-a774-e9c1c415f424/cluster
  - resource_id: /aws/xyz/idem-test-cluster-11d35ba3-68fe-436a-a774-e9c1c415f424/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-11d35ba3-68fe-436a-a774-e9c1c415f424/cluster:*


/aws/xyz/idem-test-cluster-1566097b-fcb5-488e-b309-c58fa7707274/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-1566097b-fcb5-488e-b309-c58fa7707274/cluster
  - resource_id: /aws/xyz/idem-test-cluster-1566097b-fcb5-488e-b309-c58fa7707274/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-1566097b-fcb5-488e-b309-c58fa7707274/cluster:*


/aws/xyz/idem-test-cluster-16c5ed32-5c2e-4a7e-8268-abe966aaee28/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-16c5ed32-5c2e-4a7e-8268-abe966aaee28/cluster
  - resource_id: /aws/xyz/idem-test-cluster-16c5ed32-5c2e-4a7e-8268-abe966aaee28/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-16c5ed32-5c2e-4a7e-8268-abe966aaee28/cluster:*


/aws/xyz/idem-test-cluster-1949714a-f80b-42ea-b655-0b4342f44878/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-1949714a-f80b-42ea-b655-0b4342f44878/cluster
  - resource_id: /aws/xyz/idem-test-cluster-1949714a-f80b-42ea-b655-0b4342f44878/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-1949714a-f80b-42ea-b655-0b4342f44878/cluster:*


/aws/xyz/idem-test-cluster-1aee439f-2f8c-4f02-8d85-e296810bd4bb/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-1aee439f-2f8c-4f02-8d85-e296810bd4bb/cluster
  - resource_id: /aws/xyz/idem-test-cluster-1aee439f-2f8c-4f02-8d85-e296810bd4bb/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-1aee439f-2f8c-4f02-8d85-e296810bd4bb/cluster:*


/aws/xyz/idem-test-cluster-1d3d023e-5ba3-4720-8c15-b94d4aa103bc/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-1d3d023e-5ba3-4720-8c15-b94d4aa103bc/cluster
  - resource_id: /aws/xyz/idem-test-cluster-1d3d023e-5ba3-4720-8c15-b94d4aa103bc/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-1d3d023e-5ba3-4720-8c15-b94d4aa103bc/cluster:*


/aws/xyz/idem-test-cluster-26288cb0-48e6-4077-bdc4-be0b4684453d/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-26288cb0-48e6-4077-bdc4-be0b4684453d/cluster
  - resource_id: /aws/xyz/idem-test-cluster-26288cb0-48e6-4077-bdc4-be0b4684453d/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-26288cb0-48e6-4077-bdc4-be0b4684453d/cluster:*


/aws/xyz/idem-test-cluster-2b19057c-5581-43c8-ac40-21a413564f88/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-2b19057c-5581-43c8-ac40-21a413564f88/cluster
  - resource_id: /aws/xyz/idem-test-cluster-2b19057c-5581-43c8-ac40-21a413564f88/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-2b19057c-5581-43c8-ac40-21a413564f88/cluster:*


/aws/xyz/idem-test-cluster-31350145-d68e-415e-9254-3649936fc742/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-31350145-d68e-415e-9254-3649936fc742/cluster
  - resource_id: /aws/xyz/idem-test-cluster-31350145-d68e-415e-9254-3649936fc742/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-31350145-d68e-415e-9254-3649936fc742/cluster:*


/aws/xyz/idem-test-cluster-37333243-7662-4151-a077-36ae32289387/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-37333243-7662-4151-a077-36ae32289387/cluster
  - resource_id: /aws/xyz/idem-test-cluster-37333243-7662-4151-a077-36ae32289387/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-37333243-7662-4151-a077-36ae32289387/cluster:*


/aws/xyz/idem-test-cluster-3a246242-381e-4785-a403-f62ae9d4ef37/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-3a246242-381e-4785-a403-f62ae9d4ef37/cluster
  - resource_id: /aws/xyz/idem-test-cluster-3a246242-381e-4785-a403-f62ae9d4ef37/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-3a246242-381e-4785-a403-f62ae9d4ef37/cluster:*


/aws/xyz/idem-test-cluster-3b36eb9d-114e-4719-a3dc-f00d696bb494/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-3b36eb9d-114e-4719-a3dc-f00d696bb494/cluster
  - resource_id: /aws/xyz/idem-test-cluster-3b36eb9d-114e-4719-a3dc-f00d696bb494/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-3b36eb9d-114e-4719-a3dc-f00d696bb494/cluster:*


/aws/xyz/idem-test-cluster-3da4c6a7-e30a-4a37-8cce-941f001b8f34/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-3da4c6a7-e30a-4a37-8cce-941f001b8f34/cluster
  - resource_id: /aws/xyz/idem-test-cluster-3da4c6a7-e30a-4a37-8cce-941f001b8f34/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-3da4c6a7-e30a-4a37-8cce-941f001b8f34/cluster:*


/aws/xyz/idem-test-cluster-426fdf9a-f445-45ec-a593-675fdc65088c/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-426fdf9a-f445-45ec-a593-675fdc65088c/cluster
  - resource_id: /aws/xyz/idem-test-cluster-426fdf9a-f445-45ec-a593-675fdc65088c/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-426fdf9a-f445-45ec-a593-675fdc65088c/cluster:*


/aws/xyz/idem-test-cluster-43d671a2-a09f-420e-b1de-26508936f141/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-43d671a2-a09f-420e-b1de-26508936f141/cluster
  - resource_id: /aws/xyz/idem-test-cluster-43d671a2-a09f-420e-b1de-26508936f141/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-43d671a2-a09f-420e-b1de-26508936f141/cluster:*


/aws/xyz/idem-test-cluster-4526993b-9e66-4215-bf5f-3c23302e25b7/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-4526993b-9e66-4215-bf5f-3c23302e25b7/cluster
  - resource_id: /aws/xyz/idem-test-cluster-4526993b-9e66-4215-bf5f-3c23302e25b7/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-4526993b-9e66-4215-bf5f-3c23302e25b7/cluster:*


/aws/xyz/idem-test-cluster-59efc692-b8ac-4d17-a172-abbb105e49e1/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-59efc692-b8ac-4d17-a172-abbb105e49e1/cluster
  - resource_id: /aws/xyz/idem-test-cluster-59efc692-b8ac-4d17-a172-abbb105e49e1/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-59efc692-b8ac-4d17-a172-abbb105e49e1/cluster:*


/aws/xyz/idem-test-cluster-5ff4ac80-33db-4ae3-a272-e277cf7b2139/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-5ff4ac80-33db-4ae3-a272-e277cf7b2139/cluster
  - resource_id: /aws/xyz/idem-test-cluster-5ff4ac80-33db-4ae3-a272-e277cf7b2139/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-5ff4ac80-33db-4ae3-a272-e277cf7b2139/cluster:*


/aws/xyz/idem-test-cluster-668ac604-d112-42bd-a52d-5b6c2b8446be/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-668ac604-d112-42bd-a52d-5b6c2b8446be/cluster
  - resource_id: /aws/xyz/idem-test-cluster-668ac604-d112-42bd-a52d-5b6c2b8446be/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-668ac604-d112-42bd-a52d-5b6c2b8446be/cluster:*


/aws/xyz/idem-test-cluster-670d31d6-eb7f-47e7-9122-90ea4402785d/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-670d31d6-eb7f-47e7-9122-90ea4402785d/cluster
  - resource_id: /aws/xyz/idem-test-cluster-670d31d6-eb7f-47e7-9122-90ea4402785d/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-670d31d6-eb7f-47e7-9122-90ea4402785d/cluster:*


/aws/xyz/idem-test-cluster-679deb50-dbd1-4a78-ab33-afc27cb4870f/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-679deb50-dbd1-4a78-ab33-afc27cb4870f/cluster
  - resource_id: /aws/xyz/idem-test-cluster-679deb50-dbd1-4a78-ab33-afc27cb4870f/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-679deb50-dbd1-4a78-ab33-afc27cb4870f/cluster:*


/aws/xyz/idem-test-cluster-752fd328-c4c8-42ec-8f04-5b4084724f7e/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-752fd328-c4c8-42ec-8f04-5b4084724f7e/cluster
  - resource_id: /aws/xyz/idem-test-cluster-752fd328-c4c8-42ec-8f04-5b4084724f7e/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-752fd328-c4c8-42ec-8f04-5b4084724f7e/cluster:*


/aws/xyz/idem-test-cluster-7e48176c-724d-4318-8a09-bef0febe28f9/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-7e48176c-724d-4318-8a09-bef0febe28f9/cluster
  - resource_id: /aws/xyz/idem-test-cluster-7e48176c-724d-4318-8a09-bef0febe28f9/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-7e48176c-724d-4318-8a09-bef0febe28f9/cluster:*


/aws/xyz/idem-test-cluster-8b55d388-3942-4b9a-aa3b-2054d295e6fa/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-8b55d388-3942-4b9a-aa3b-2054d295e6fa/cluster
  - resource_id: /aws/xyz/idem-test-cluster-8b55d388-3942-4b9a-aa3b-2054d295e6fa/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-8b55d388-3942-4b9a-aa3b-2054d295e6fa/cluster:*


/aws/xyz/idem-test-cluster-8f258a9b-2c9a-4677-83e4-1538df53ab97/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-8f258a9b-2c9a-4677-83e4-1538df53ab97/cluster
  - resource_id: /aws/xyz/idem-test-cluster-8f258a9b-2c9a-4677-83e4-1538df53ab97/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-8f258a9b-2c9a-4677-83e4-1538df53ab97/cluster:*


/aws/xyz/idem-test-cluster-9496cf64-d683-4719-adf6-04d87ce7ef15/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-9496cf64-d683-4719-adf6-04d87ce7ef15/cluster
  - resource_id: /aws/xyz/idem-test-cluster-9496cf64-d683-4719-adf6-04d87ce7ef15/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-9496cf64-d683-4719-adf6-04d87ce7ef15/cluster:*


/aws/xyz/idem-test-cluster-96857098-1e20-4084-92b3-c82f1c6c0d12/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-96857098-1e20-4084-92b3-c82f1c6c0d12/cluster
  - resource_id: /aws/xyz/idem-test-cluster-96857098-1e20-4084-92b3-c82f1c6c0d12/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-96857098-1e20-4084-92b3-c82f1c6c0d12/cluster:*


/aws/xyz/idem-test-cluster-9bde6473-84f2-447d-90d1-5b0ef19c1a9c/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-9bde6473-84f2-447d-90d1-5b0ef19c1a9c/cluster
  - resource_id: /aws/xyz/idem-test-cluster-9bde6473-84f2-447d-90d1-5b0ef19c1a9c/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-9bde6473-84f2-447d-90d1-5b0ef19c1a9c/cluster:*


/aws/xyz/idem-test-cluster-a0ea9655-0f36-40ab-889a-309c051b5afe/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-a0ea9655-0f36-40ab-889a-309c051b5afe/cluster
  - resource_id: /aws/xyz/idem-test-cluster-a0ea9655-0f36-40ab-889a-309c051b5afe/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-a0ea9655-0f36-40ab-889a-309c051b5afe/cluster:*


/aws/xyz/idem-test-cluster-a40b5243-1470-4635-937e-acfa940d57fd/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-a40b5243-1470-4635-937e-acfa940d57fd/cluster
  - resource_id: /aws/xyz/idem-test-cluster-a40b5243-1470-4635-937e-acfa940d57fd/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-a40b5243-1470-4635-937e-acfa940d57fd/cluster:*


/aws/xyz/idem-test-cluster-acb50041-e009-447a-9d19-62e93035441d/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-acb50041-e009-447a-9d19-62e93035441d/cluster
  - resource_id: /aws/xyz/idem-test-cluster-acb50041-e009-447a-9d19-62e93035441d/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-acb50041-e009-447a-9d19-62e93035441d/cluster:*


/aws/xyz/idem-test-cluster-b1a04e08-75b0-45ea-b6e9-88fc15e5f2c0/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-b1a04e08-75b0-45ea-b6e9-88fc15e5f2c0/cluster
  - resource_id: /aws/xyz/idem-test-cluster-b1a04e08-75b0-45ea-b6e9-88fc15e5f2c0/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-b1a04e08-75b0-45ea-b6e9-88fc15e5f2c0/cluster:*


/aws/xyz/idem-test-cluster-b1e6c99b-e9a3-4dc7-b1a8-0fba4d4f0801/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-b1e6c99b-e9a3-4dc7-b1a8-0fba4d4f0801/cluster
  - resource_id: /aws/xyz/idem-test-cluster-b1e6c99b-e9a3-4dc7-b1a8-0fba4d4f0801/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-b1e6c99b-e9a3-4dc7-b1a8-0fba4d4f0801/cluster:*


/aws/xyz/idem-test-cluster-b4a1f936-16bb-4c4a-b2eb-d0a5113ee885/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-b4a1f936-16bb-4c4a-b2eb-d0a5113ee885/cluster
  - resource_id: /aws/xyz/idem-test-cluster-b4a1f936-16bb-4c4a-b2eb-d0a5113ee885/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-b4a1f936-16bb-4c4a-b2eb-d0a5113ee885/cluster:*


/aws/xyz/idem-test-cluster-b6392dc2-3491-451c-9032-12af32e35a13/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-b6392dc2-3491-451c-9032-12af32e35a13/cluster
  - resource_id: /aws/xyz/idem-test-cluster-b6392dc2-3491-451c-9032-12af32e35a13/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-b6392dc2-3491-451c-9032-12af32e35a13/cluster:*


/aws/xyz/idem-test-cluster-bf6ba4b7-5ea5-436b-be81-3dd8d02ad99b/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-bf6ba4b7-5ea5-436b-be81-3dd8d02ad99b/cluster
  - resource_id: /aws/xyz/idem-test-cluster-bf6ba4b7-5ea5-436b-be81-3dd8d02ad99b/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-bf6ba4b7-5ea5-436b-be81-3dd8d02ad99b/cluster:*


/aws/xyz/idem-test-cluster-cc2611ef-8277-43c2-b007-80aa7bc84770/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-cc2611ef-8277-43c2-b007-80aa7bc84770/cluster
  - resource_id: /aws/xyz/idem-test-cluster-cc2611ef-8277-43c2-b007-80aa7bc84770/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-cc2611ef-8277-43c2-b007-80aa7bc84770/cluster:*


/aws/xyz/idem-test-cluster-cd4280b8-f22f-438f-8ad6-d74e431604a4/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-cd4280b8-f22f-438f-8ad6-d74e431604a4/cluster
  - resource_id: /aws/xyz/idem-test-cluster-cd4280b8-f22f-438f-8ad6-d74e431604a4/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-cd4280b8-f22f-438f-8ad6-d74e431604a4/cluster:*


/aws/xyz/idem-test-cluster-d094b3ab-4de4-496f-a198-cf0bedc0a749/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-d094b3ab-4de4-496f-a198-cf0bedc0a749/cluster
  - resource_id: /aws/xyz/idem-test-cluster-d094b3ab-4de4-496f-a198-cf0bedc0a749/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-d094b3ab-4de4-496f-a198-cf0bedc0a749/cluster:*


/aws/xyz/idem-test-cluster-d6e0602b-b135-41cf-a67a-bfaa1123fec1/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-d6e0602b-b135-41cf-a67a-bfaa1123fec1/cluster
  - resource_id: /aws/xyz/idem-test-cluster-d6e0602b-b135-41cf-a67a-bfaa1123fec1/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-d6e0602b-b135-41cf-a67a-bfaa1123fec1/cluster:*


/aws/xyz/idem-test-cluster-daaedc96-a9de-42aa-861b-311d5dd61a22/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-daaedc96-a9de-42aa-861b-311d5dd61a22/cluster
  - resource_id: /aws/xyz/idem-test-cluster-daaedc96-a9de-42aa-861b-311d5dd61a22/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-daaedc96-a9de-42aa-861b-311d5dd61a22/cluster:*


/aws/xyz/idem-test-cluster-dd7aed7c-1293-4908-8331-081f92ca6baf/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-dd7aed7c-1293-4908-8331-081f92ca6baf/cluster
  - resource_id: /aws/xyz/idem-test-cluster-dd7aed7c-1293-4908-8331-081f92ca6baf/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-dd7aed7c-1293-4908-8331-081f92ca6baf/cluster:*


/aws/xyz/idem-test-cluster-eae48d5d-3fea-419e-9de1-4823dbec590e/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-eae48d5d-3fea-419e-9de1-4823dbec590e/cluster
  - resource_id: /aws/xyz/idem-test-cluster-eae48d5d-3fea-419e-9de1-4823dbec590e/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-eae48d5d-3fea-419e-9de1-4823dbec590e/cluster:*


/aws/xyz/idem-test-cluster-f34eff5a-dbe6-47a6-a71f-de0f75921cdd/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster-f34eff5a-dbe6-47a6-a71f-de0f75921cdd/cluster
  - resource_id: /aws/xyz/idem-test-cluster-f34eff5a-dbe6-47a6-a71f-de0f75921cdd/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster-f34eff5a-dbe6-47a6-a71f-de0f75921cdd/cluster:*


/aws/xyz/idem-test-cluster/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test-cluster/cluster
  - resource_id: /aws/xyz/idem-test-cluster/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test-cluster/cluster:*


/aws/xyz/idem-test/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/idem-test/cluster
  - resource_id: /aws/xyz/idem-test/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/idem-test/cluster:*


/aws/xyz/xyz-cluster-testing-order/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/xyz-cluster-testing-order/cluster
  - resource_id: /aws/xyz/xyz-cluster-testing-order/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/xyz-cluster-testing-order/cluster:*


/aws/xyz/xyz-cluster-testing-order1/cluster:
  aws.cloudwatch.log_group.present:
  - name: /aws/xyz/xyz-cluster-testing-order1/cluster
  - resource_id: /aws/xyz/xyz-cluster-testing-order1/cluster
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:/aws/xyz/xyz-cluster-testing-order1/cluster:*


RDSOSMetrics:
  aws.cloudwatch.log_group.present:
  - name: RDSOSMetrics
  - resource_id: RDSOSMetrics
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:RDSOSMetrics:*


xyz-idem-test_redlock_flow_log_group:
  aws.cloudwatch.log_group.present:
  - name: xyz-idem-test_redlock_flow_log_group
  - resource_id: xyz-idem-test_redlock_flow_log_group
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - tags:
      Automation: 'true'
      COGS: OPEX
      Environment: test-dev
      KubernetesCluster: idem-test
      Owner: org1
