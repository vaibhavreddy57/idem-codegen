include:
- sls.credstash-kms-key
- sls.eks-admin-role
- sls.eks-worker-iam-role
- sls.provider
- sls.tags
- sls.versions
